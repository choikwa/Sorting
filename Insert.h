/*
 * Insert.h
 *
 *  Created on: Jan 29, 2015
 *      Author: thinkdoge
 */

#ifndef INSERT_H_
#define INSERT_H_

#include <iostream>
using namespace std;
class Insert
{
public:
  Insert();
  virtual ~Insert();
  template <class T, class F>
  int perform(T begin, T end, F cmp)
  {
    /* http://en.wikipedia.org/wiki/Insertion_sort
     *
     * for i ← 1 to length(A) - 1
     *    j ← i
     *    while j > 0 and A[j-1] > A[j]
     *        swap A[j] and A[j-1]
     *        j ← j - 1
     */
    for(T it = next(begin); it != end; it++)
    {
      T jt = it;
      while(jt != begin && cmp(*jt, *prev(jt)))
      {
        auto tmp = *jt;
        *jt = *prev(jt);
        *prev(jt) = tmp;
        jt--;
      }
    }
    return 0;
  };

  template <class T, class F>
  int performFast(T begin, T end, F cmp)
  {
    /* http://en.wikipedia.org/wiki/Insertion_sort
     * for i = 1 to length(A) - 1
     *    x = A[i]
     *    j = i
     *    while j > 0 and A[j-1] > x
     *        A[j] = A[j-1]
     *        j = j - 1
     *    A[j] = x
     */
    for(T it = next(begin); it != end; it++)
    {
      auto x = *it;
      T jt = it;
      while(jt != begin && cmp(x, *prev(jt)))
      {
        *jt = *prev(jt);
        jt--;
      }
      *jt = x;
    }
    return 0;
  }

  template <class T, class F>
  int performFaster(T &list, F cmp)
  {
    /*
     * for i = 1 to length(A) - 1
     *   j = i - 1
     *   bool b
     *   while (b = A[j] > A[i]) and j > 0
     *     j--;
     *   if b == False
     *     j++;
     *   insert A[i] at j
     *   Erase A[i]
     */
    for(auto it = next(list.begin()); it != list.end();)
    {
      auto jt = prev(it);
      bool cmpRes;
      while((cmpRes = cmp(*it, *jt)) && jt != list.begin())
      {
        jt--;
      }
      if(!cmpRes)
        jt++;
      list.insert(jt, *it);
      it = list.erase(it);
    }

    return 0;
  }

};

#endif /* INSERT_H_ */

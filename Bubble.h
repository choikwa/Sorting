/*
 * Bubble.h
 *
 *  Created on: Jan 29, 2015
 *      Author: thinkdoge
 */

#ifndef BUBBLE_H_
#define BUBBLE_H_

class Bubble
{
public:
  Bubble();
  virtual ~Bubble();
};

#endif /* BUBBLE_H_ */

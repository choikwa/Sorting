/*
 * main.cpp
 *
 *  Created on: Jan 29, 2015
 *      Author: thinkdoge
 */

#include <iostream>
#include <random>
#include <list>
#include <deque>
#include <vector>
#include <ctime>
#include <chrono>
#include <cstdlib>

#include "Insert.h"
#include "STL.h"

using namespace std;

template <class C>
void printContainer(C &c)
{
  for(auto &elem: c)
    cout << elem << " ";
  cout << "\n\n";
}
template <class T>
void printContainer(T begin, T end)
{
  for(auto it = begin; it != end; it++)
    cout << *it << " ";
  cout << "\n\n";
}

template <class Sorter, class Container, class Lambda>
void perfMeasureSorter(string s, Sorter &str, Container ctr, Lambda F)
{
  //printContainer(ctr);
  auto t_start = chrono::high_resolution_clock::now();
  str.perform(ctr.begin(), ctr.end(), F);
  auto t_end = chrono::high_resolution_clock::now();
  cout << s << "(" << chrono::duration<double, milli>(t_end - t_start).count() << " ms)" << endl;
  //printContainer(ctr);
}
template <class Sorter, class T, class Lambda>
void perfMeasureSorter1(string s, Sorter &str, T begin, T end, Lambda F)
{
  //printContainer(ctr);
  auto t_start = chrono::high_resolution_clock::now();
  str.performFast(begin, end, F);
  auto t_end = chrono::high_resolution_clock::now();
  cout << s << "(" << chrono::duration<double, milli>(t_end - t_start).count() << " ms)" << endl;
  //printContainer(begin, end);
}
void perfMeasureSorter2(string s, list<int> ll)
{
  auto t_start = chrono::high_resolution_clock::now();
  ll.sort();
  auto t_end = chrono::high_resolution_clock::now();
  cout << s << "(" << chrono::duration<double, milli>(t_end - t_start).count() << " ms)" << endl;
  //printContainer(ll);
}

int main()
{

  const int SIZE = 2500;
  const int MAX = 10000;

  srand(time(NULL));
  list<int> ll;
  for(int i=0; i<SIZE; i++)
    ll.push_back(rand() % MAX);

  deque<int> dq;
  for(int i=0; i<SIZE; i++)
    dq.push_back(rand() % MAX);

  vector<int> vec;
  for(int i=0; i<SIZE; i++)
    vec.push_back(rand() % MAX);

  auto less = [&](const int &l, const int &r){ return l < r; };
  Insert insert;
  STL stl;

  perfMeasureSorter(string("insertion sort"), insert, ll, less);
  list<int> ll1(ll);
  perfMeasureSorter1("insertion sort (fast)", insert, ll1.begin(), ll1.end(), less);

  list<int> ll2(ll);
  auto t_start = chrono::high_resolution_clock::now();
  insert.performFaster(ll2, less);
  auto t_end = chrono::high_resolution_clock::now();
  cout << "insertion sort (faster) (" << chrono::duration<double, milli>(t_end - t_start).count() << " ms)" << endl;

  perfMeasureSorter2(string("std::list<int>::sort"), ll);
  perfMeasureSorter(string("std::sort vec"), stl, vec, less);
  perfMeasureSorter(string("std::sort dq"), stl, dq, less);

  //insert.perform
}



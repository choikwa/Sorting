/*
 * STL.h
 *
 *  Created on: Jan 30, 2015
 *      Author: thinkdoge
 */

#ifndef STL_H_
#define STL_H_

#include <algorithm>

class STL
{
public:
  STL();
  virtual ~STL();
  template <class T, class F>
  int perform(T begin, T end, F cmp)
  {
    std::sort(begin, end, cmp);
    return 0;
  }
};

#endif /* STL_H_ */
